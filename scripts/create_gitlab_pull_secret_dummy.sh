#!/bin/bash

NAMESPACE="wavefier-stable"

# https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/
REGISTRY_USERNAME="dummy"
REGISTRY_PASSWORD="dummy"

BASE_64_BASIC_AUTH_CREDENTIALS=`echo -n "${REGISTRY_USERNAME}:${REGISTRY_PASSWORD}" | base64`


cat << EOF > .dockerconfigjson
{
    "auths": {
        "https://registry.gitlab.com":{
            "username":"$REGISTRY_USERNAME",
            "password":"$REGISTRY_PASSWORD",
            "email":"albert.einstein@ligo.org",
            "auth":"$BASE_64_BASIC_AUTH_CREDENTIALS"
    	}
    }
}
EOF

BASE_64_ENCODED_DOCKER_FILE=`cat .dockerconfigjson | base64`

cat << EOF > gitlab-pull-secret-wavefier.yaml 
---
kind: Secret
apiVersion: v1
metadata:
  name: gitlab-pull-secret-wavefier
  namespace: $NAMESPACE
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: "$BASE_64_ENCODED_DOCKER_FILE"
EOF

kubectl apply -f gitlab-pull-secret-wavefier.yaml
