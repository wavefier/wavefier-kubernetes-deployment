*********************
Continuous deployment
*********************

Continuous deployment is implemented in the file *.gitlab-ci.yaml*. Edit this file to enable/disable the installation of individual components, using the following configuration variables:

.. highlight:: yaml
.. code-block:: yaml

   variables:
     DEPLOY: "true" # to deploy all components set to "true" below
     UNDEPLOY: "false" # to undeploy all components set to "true" below
     RUCIO_IMPORTER: "false" 
     RAW_IMPORTER: "true"
     WDF: "true"
     TRIGGER_HANDLERS: "true"
     ML: "false"

