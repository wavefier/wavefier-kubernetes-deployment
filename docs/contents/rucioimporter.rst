************************************
Rucio Importer (optional) deployment
************************************

Deployment with one replica that mounts a shared PersistentVolumeClaim of 9 GiB. This can be used to retrieve data from the ESCAPE DataLake. Data is stored in the shared voulme and can be retrieved by the Offline Importer. The name of an existing PersistentVolumeClaim can be specified, otherwise the chart creates a new one.
Robot certificate and key used to authenticate to Rucio should go in the *certificates* folder.
To install the Helm chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh rucio-importer

To uninstall the chart, first edit the delete script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/delete.sh rucio-importer

Note that the PersistentVolume is deleted on chart deletion.

