***************************
Trigger Handlers deployment
***************************

The chart creates a Deployment with one replica.
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh trigger-handlers

To uninstall the chart, first edit the delete script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/delete.sh trigger-handlers

The configuration is modified by editing the *triggerHandlers* section of the *config.yaml* file.



