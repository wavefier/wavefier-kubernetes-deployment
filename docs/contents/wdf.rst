**************
WDF deployment
**************

The chart creates a Deployment with one replica.
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh wdf

To uninstall the chart, first edit the delete script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/delete.sh wdf

The configuration is modified by editing the *wdf* section of the *config.yaml* file.


