*************
Initial setup
*************

In order to pull the Wavefier images from Gitlab private registries, you need to have a GitLab deploy token with capability *read_registry* associated to the group Wavefier.
You should insert the token name and value, as variables REGISTRY_USERNAME and REGISTRY_PASSWORD respectively, in the script: scripts/create_gitlab_pull_secret.sh. 
Run the script in order to create the Kubernetes Secret that will be used by the various Helm charts:

.. highlight:: bash
.. code-block:: bash

   ./scripts/create_gitlab_pull_secret.sh

All charts share a common configuration file: **config.yaml**. Edit this file to configure the various Wavefier components.

