***************************
Machine Learning deployment
***************************

The chart creates a Deployment with one replica.
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh ml

To uninstall the chart, first edit the delete script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/delete.sh ml

The configuration is modified by editing the *ml* section of the *config.yaml* file.



