***************************
Offline Importer deployment
***************************

This importer publishes data present in a local folder to the Wavefier Kafka broker.
A separate component, i.e. the Rucio Importer or the Kafka Importer, should populate the local folder with data. 
We foresee two alternative deployment startegies:

* **Sidecar container:** the container of the additional importer that retrieves the data, runs in the same Pod as the offline importer. The two share an *in memory* volume space (deleted on chart deletion). This configuration is the most efficient and straightforward, but it requires an amount of memory at least as large as the required volume size, on a single node. Example configuration:

.. highlight:: yaml
.. code-block:: yaml  

   ...
   sharedVolumeCapacity: 9Gi
   ...
   rawImporter:
   ...
     sharedVolume:
       enabled: false
     ...   
     sidecarContainer:
       enabled: true
     ...

The configuration file provides an example configuration using the Kafka Importer as sidecar container.

* **Shared persistent volume:** the Kubernetes cluster should be able to provide volumes of the type RWX (ReadWriteMany). The offline importer Pod mounts a PersistentVolume shared with the separate data retrieving importer. The name of an existing PersistentVolumeClaim can be specified, otherwise the chart creates a new one. The volume is deleted upon chart deletion. With this deployment strategy one should also run a separate instance of a data retrieving importer, see for example the Rucio Importer in the next paragraph. Example configuration:

.. highlight:: yaml
.. code-block:: yaml  

   ...
   sharedVolumeCapacity: 9Gi
   ...
   rawImporter:
   ...
     sharedVolume:
       enabled: true
     ...
     sidecarContainer:
       enabled: false
     ...

The configuration is modified by editing the *rawImporter* section of the *config.yaml* file.
By default, the Helm chart creates a Deployment with one replica. 
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh raw-importer

To uninstall the chart, first edit the delete script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/delete.sh raw-importer


