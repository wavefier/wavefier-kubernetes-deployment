.. wavefier-kubernetes-deployment documentation master file, created by
   sphinx-quickstart on Tue Mar 15 14:51:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wavefier-kubernetes-deployment's documentation!
==========================================================
Files needed to deploy Wavefier on Kubernetes (tested on the CNAF cluster).

.. toctree::
   :maxdepth: 2
   :caption: Table of contents

   contents/setup.rst
   contents/rawimporter.rst
   contents/rucioimporter.rst
   contents/wdf.rst
   contents/triggers.rst
   contents/ml.rst
   contents/cd.rst
