# Wavefier Kubernetes Deployment

Files needed to deploy Wavefier on the Kubernetes cluster at CNAF, using a microservices approach.

The complete documentation can be found here: https://wavefier.gitlab.io/wavefier-kubernetes-deployment/
